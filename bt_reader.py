#!/usr/bin/env python3
# -*- coding: utf8 -*-
# coding: utf8
#
# Read BT serial for JSON formated sensor data
#  and put that into database (cloud?, terminal?)
#


import serial
import json
import sqlite3 as sqlite
import math

db = sqlite.connect( 'iot.sqlite' )
cur = db.cursor()
debug = False 
read_str = ""
old_str = ""
file_read = False

if (debug):                     #use file as debug
        ser = open('rfcomm1.log')
else:
        ser = serial.Serial('/dev/rfcomm0', 9600, timeout=10)






# Check if taple already exist
def checkIfTableExist(tableName):
        # Check that debug DataBase table exist
        cur.execute('SELECT name FROM sqlite_master WHERE type="table" AND name=:name ;', {'name': tableName})
        tableExist = cur.fetchall()
        if (len(tableExist) > 0):
                return True
        else:
                return False
#
#
# Parse result and return it
def createDatabase():
        if not ( checkIfTableExist('Mansikka') ):
                #Create table if it doesn't exist
                cur.execute( 'CREATE  TABLE  IF NOT EXISTS "main"."Mansikka" (\
                "id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ,\
                "dirt" INTEGER , \
                "timing" INTEGER , \
                "datetime" DATETIME NOT NULL  DEFAULT (DATETIME()) , \
                "pressure" FLOAT, \
                "temp1" FLOAT, \
                "temp2" FLOAT , \
                "dew_point" FLOAT, \
                "huminity" FLOAT , \
                "light" INTEGER , \
                "ext_pin" INTEGER );' )
                db.commit()
                print( "[DB][warning] Table 'Manga' created to DB!" )
        #

createDatabase()



def readSerial(amount=200):
        global file_read
        global ser
        if (debug):
                if not (file_read):
                        s = ser.read(amount)
                else:
                        return ""
                if (len(s) < amount):
                        file_read = True
                        ser.close()
        else:
                s = ser.read(amount)
        return str(s)




''' Func to trying to fix broken data

'''
def tryFixingData(dat=""):
        #1
        dat = dat.replace('"irt"', '"dirt"')
        dat = dat.replace('"drt"', '"dirt"')
        dat = dat.replace('"dit"', '"dirt"')
        dat = dat.replace('"dir"', '"dirt"')
        dat = dat.replace(' "dirt:', ' "dirt":')
        dat = dat.replace(' dirt":', ' "dirt":')
        dat = dat.replace(' "dirt"1', ' "dirt":1')
        dat = dat.replace(' "dirt"2', ' "dirt":2')
        dat = dat.replace(' "dirt"3', ' "dirt":3')
        dat = dat.replace(' "dirt"4', ' "dirt":4')
        dat = dat.replace(' "dirt"5', ' "dirt":5')
        dat = dat.replace(' "dirt"6', ' "dirt":6')
        dat = dat.replace(' "dirt"7', ' "dirt":7')
        dat = dat.replace(' "dirt"8', ' "dirt":8')
        dat = dat.replace(' "dirt"9', ' "dirt":9')
        dat = dat.replace(' "dirt"0', ' "dirt":0')
        dat = dat.replace(' "dirt":00,', ' "dirt":null,')
        dat = dat.replace(' "dirt":01,', ' "dirt":null,')
        dat = dat.replace(' "dirt":02,', ' "dirt":null,')
        dat = dat.replace(' "dirt":03,', ' "dirt":null,')
        dat = dat.replace(' "dirt":04,', ' "dirt":null,')
        dat = dat.replace(' "dirt":05,', ' "dirt":null,')
        dat = dat.replace(' "dirt":06,', ' "dirt":null,')
        dat = dat.replace(' "dirt":07,', ' "dirt":null,')
        dat = dat.replace(' "dirt":08,', ' "dirt":null,')
        dat = dat.replace(' "dirt":09,', ' "dirt":null,')
        #2
        dat = dat.replace('"iming"', '"timing"')
        dat = dat.replace('"tming"', '"timing"')
        dat = dat.replace('"tiing"', '"timing"')
        dat = dat.replace('"timng"', '"timing"')
        dat = dat.replace('"timig"', '"timing"')
        dat = dat.replace('"timin"', '"timing"')
        #
        dat = dat.replace(' "timing:', ' "timing":')
        dat = dat.replace(' timing":', ' "timing":')
        dat = dat.replace(' "timing"2', ' "timing":2')
        #3
        dat = dat.replace('"atetime"', '"datetime"')
        dat = dat.replace('"dtetime"', '"datetime"')
        dat = dat.replace('"daetime"', '"datetime"')
        dat = dat.replace('"dattime"', '"datetime"')
        dat = dat.replace('"dateime"', '"datetime"')
        dat = dat.replace('"datetme"', '"datetime"')
        dat = dat.replace('"datetie"', '"datetime"')
        dat = dat.replace('"datetim"', '"datetime"')
        #3.5
        dat = dat.replace('"017-06-11 ', '"2017-06-11 ')
        dat = dat.replace('"017-06-10 ', '"2017-06-10 ')
        dat = dat.replace('"017-06-09 ', '"2017-06-09 ')
        dat = dat.replace('"217-06-11 ', '"2017-06-11 ')
        dat = dat.replace('"217-06-10 ', '"2017-06-10 ')
        dat = dat.replace('"217-06-09 ', '"2017-06-09 ')
        dat = dat.replace('"207-06-11 ', '"2017-06-11 ')
        dat = dat.replace('"207-06-10 ', '"2017-06-10 ')
        dat = dat.replace('"207-06-09 ', '"2017-06-09 ')
        dat = dat.replace('"201-06-11 ', '"2017-06-11 ')
        dat = dat.replace('"201-06-10 ', '"2017-06-10 ')
        dat = dat.replace('"201-06-09 ', '"2017-06-09 ')
        dat = dat.replace('"201706-11 ', '"2017-06-11 ')
        dat = dat.replace('"201706-10 ', '"2017-06-10 ')
        dat = dat.replace('"201706-09 ', '"2017-06-09 ')
        dat = dat.replace('"2017-6-11 ', '"2017-06-11 ')
        dat = dat.replace('"2017-6-10 ', '"2017-06-10 ')
        dat = dat.replace('"2017-6-09 ', '"2017-06-09 ')
        dat = dat.replace('"2017-0-11 ', '"2017-06-11 ')
        dat = dat.replace('"2017-0-10 ', '"2017-06-10 ')
        dat = dat.replace('"2017-0-09 ', '"2017-06-09 ')
        dat = dat.replace('"2017-0611 ', '"2017-06-11 ')
        dat = dat.replace('"2017-0610 ', '"2017-06-10 ')
        dat = dat.replace('"2017-0609 ', '"2017-06-09 ')
        dat = dat.replace('"2017-06-0 ', '"2017-06-10 ')
        dat = dat.replace('"2017-06-9 ', '"2017-06-09 ')
        #
        for i in range(10):
                dat = dat.replace('"2017-06-09'+str(i), '"2017-06-09 '+str(i))
                dat = dat.replace('"2017-06-10'+str(i), '"2017-06-10 '+str(i))
                dat = dat.replace('"2017-06-11'+str(i), '"2017-06-11 '+str(i))
        #
        dat = dat.replace(' "datetime:', ' "datetime":')
        dat = dat.replace(' datetime":', ' "datetime":')
        dat = dat.replace(' "datetime""', ' "datetime":"')
        dat = dat.replace(' "datetime":2', ' "datetime":"2')
        dat = dat.replace('0,\n "pressure', '0",\n "pressure')
        dat = dat.replace('1,\n "pressure', '1",\n "pressure')
        dat = dat.replace('2,\n "pressure', '2",\n "pressure')
        dat = dat.replace('3,\n "pressure', '3",\n "pressure')
        dat = dat.replace('4,\n "pressure', '4",\n "pressure')
        dat = dat.replace('5,\n "pressure', '5",\n "pressure')
        dat = dat.replace('6,\n "pressure', '6",\n "pressure')
        dat = dat.replace('7,\n "pressure', '7",\n "pressure')
        dat = dat.replace('8,\n "pressure', '8",\n "pressure')
        dat = dat.replace('9,\n "pressure', '9",\n "pressure')
        #4
        dat = dat.replace('"ressure"', '"pressure"')
        dat = dat.replace('"pessure"', '"pressure"')
        dat = dat.replace('"prssure"', '"pressure"')
        dat = dat.replace('"presure"', '"pressure"')
        dat = dat.replace('"pressre"', '"pressure"')
        dat = dat.replace('"pressue"', '"pressure"')
        dat = dat.replace('"pressur"', '"pressure"')
        #
        dat = dat.replace(' "pressure:', ' "pressure":')
        dat = dat.replace(' pressure":', ' "pressure":')
        dat = dat.replace(' "pressure"1', ' "pressure":1')
        dat = dat.replace(' "pressure"2', ' "pressure":2')
        dat = dat.replace(' "pressure"3', ' "pressure":3')
        dat = dat.replace(' "pressure"4', ' "pressure":4')
        dat = dat.replace(' "pressure"5', ' "pressure":5')
        dat = dat.replace(' "pressure"6', ' "pressure":6')
        dat = dat.replace(' "pressure"7', ' "pressure":7')
        dat = dat.replace(' "pressure"8', ' "pressure":8')
        dat = dat.replace(' "pressure"9', ' "pressure":9')
        dat = dat.replace(' "pressure":00.', ' "pressure":100.')
        #5
        dat = dat.replace('"emp1"', '"temp1"')
        dat = dat.replace('"tmp1"', '"temp1"')
        dat = dat.replace('"tep1"', '"temp1"')
        dat = dat.replace('"tem1"', '"temp1"')
        #
        dat = dat.replace(' "temp1:', ' "temp1":')
        dat = dat.replace(' temp1":', ' "temp1":')
        dat = dat.replace(' "temp1"1', ' "temp1":1')
        dat = dat.replace(' "temp1"2', ' "temp1":2')
        dat = dat.replace(' "temp1"3', ' "temp1":3')
        dat = dat.replace(' "temp1"4', ' "temp1":4')
        dat = dat.replace(' "temp1"5', ' "temp1":5')
        dat = dat.replace(' "temp1"6', ' "temp1":6')
        dat = dat.replace(' "temp1"7', ' "temp1":7')
        dat = dat.replace(' "temp1"8', ' "temp1":8')
        dat = dat.replace(' "temp1"9', ' "temp1":9')
        dat = dat.replace(' "temp1"0', ' "temp1":0')
        #6
        dat = dat.replace('"emp2"', '"temp2"')
        dat = dat.replace('"tmp2"', '"temp2"')
        dat = dat.replace('"tep2"', '"temp2"')
        dat = dat.replace('"tem2"', '"temp2"')
        #
        dat = dat.replace(' "temp2:', ' "temp2":')
        dat = dat.replace(' temp2":', ' "temp2":')
        dat = dat.replace(' "temp2"1', ' "temp2":1')
        dat = dat.replace(' "temp2"2', ' "temp2":2')
        dat = dat.replace(' "temp2"3', ' "temp2":3')
        dat = dat.replace(' "temp2"4', ' "temp2":4')
        dat = dat.replace(' "temp2"5', ' "temp2":5')
        dat = dat.replace(' "temp2"6', ' "temp2":6')
        dat = dat.replace(' "temp2"7', ' "temp2":7')
        dat = dat.replace(' "temp2"8', ' "temp2":8')
        dat = dat.replace(' "temp2"9', ' "temp2":9')
        dat = dat.replace(' "temp2"0', ' "temp2":0')
        # 6.5
        dat = dat.replace('"emp"', '"temp"')
        dat = dat.replace('"tmp"', '"temp"')
        dat = dat.replace('"tep"', '"temp"')
        dat = dat.replace('"tem"', '"temp"')
        if ( dat.find('"temp"') != -1 ):        #find temp not 1 or 2?
                if (dat.find('"temp2"') != -1):
                        dat = dat.replace('"temp"', '"temp1"')
                elif (dat.find('"temp1"') != -1):
                        dat = dat.replace('"temp"', '"temp2"')
                else:
                        print("shit happens..")
        #7
        dat = dat.replace('"ew_point"', '"dew_point"')
        dat = dat.replace('"dw_point"', '"dew_point"')
        dat = dat.replace('"de_point"', '"dew_point"')
        dat = dat.replace('"dewpoint"', '"dew_point"')
        dat = dat.replace('"dew_oint"', '"dew_point"')
        dat = dat.replace('"dew_pint"', '"dew_point"')
        dat = dat.replace('"dew_pont"', '"dew_point"')
        dat = dat.replace('"dew_poit"', '"dew_point"')
        dat = dat.replace('"dew_poin"', '"dew_point"')
        #
        dat = dat.replace(' "dew_point:', ' "dew_point":')
        dat = dat.replace(' dew_point":', ' "dew_point":')
        dat = dat.replace(' "dew_point"6', ' "dew_point":6')
        dat = dat.replace(' "dew_point"7', ' "dew_point":7')
        dat = dat.replace(' "dew_point"8', ' "dew_point":8')
        dat = dat.replace(' "dew_point":.00,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.01,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.02,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.03,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.04,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.05,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.06,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.07,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.08,', ' "dew_point":null,')
        dat = dat.replace(' "dew_point":.09,', ' "dew_point":null,')
        for i in range(10,100):
                dat = dat.replace(' "dew_point":.'+str(i)+',', ' "dew_point":null,')
        #8
        dat = dat.replace('"uminity"', '"huminity"')
        dat = dat.replace('"hminity"', '"huminity"')
        dat = dat.replace('"huinity"', '"huminity"')
        dat = dat.replace('"humnity"', '"huminity"')
        dat = dat.replace('"humiity"', '"huminity"')
        dat = dat.replace('"huminty"', '"huminity"')
        dat = dat.replace('"huminiy"', '"huminity"')
        dat = dat.replace('"huminit"', '"huminity"')
        #
        dat = dat.replace(' "huminity:', ' "huminity":')
        dat = dat.replace(' huminity":', ' "huminity":')
        dat = dat.replace(' "huminity"1', ' "huminity":1')
        dat = dat.replace(' "huminity"2', ' "huminity":2')
        dat = dat.replace(' "huminity"3', ' "huminity":3')
        dat = dat.replace(' "huminity"4', ' "huminity":4')
        dat = dat.replace(' "huminity"5', ' "huminity":5')
        dat = dat.replace(' "huminity"6', ' "huminity":6')
        dat = dat.replace(' "huminity"7', ' "huminity":7')
        dat = dat.replace(' "huminity"8', ' "huminity":8')
        dat = dat.replace(' "huminity"9', ' "huminity":9')
        dat = dat.replace(' "huminity"0', ' "huminity":0')
        #9
        dat = dat.replace('"ight"', '"light"')
        dat = dat.replace('"lght"', '"light"')
        dat = dat.replace('"liht"', '"light"')
        dat = dat.replace('"ligt"', '"light"')
        dat = dat.replace('"ligh"', '"light"')
        #
        dat = dat.replace(' "light:', ' "light":')
        dat = dat.replace(' light":', ' "light":')
        dat = dat.replace(' "light"1', ' "light":1')
        dat = dat.replace(' "light"2', ' "light":2')
        dat = dat.replace(' "light"3', ' "light":3')
        dat = dat.replace(' "light"4', ' "light":4')
        dat = dat.replace(' "light"5', ' "light":5')
        dat = dat.replace(' "light"6', ' "light":6')
        dat = dat.replace(' "light"7', ' "light":7')
        dat = dat.replace(' "light"8', ' "light":8')
        dat = dat.replace(' "light"9', ' "light":9')
        dat = dat.replace(' "light"0', ' "light":0')
        #10
        dat = dat.replace('"ight2"', '"light2"')
        dat = dat.replace('"lght2"', '"light2"')
        dat = dat.replace('"liht2"', '"light2"')
        dat = dat.replace('"ligt2"', '"light2"')
        dat = dat.replace('"ligh2"', '"light2"')
        #
        dat = dat.replace(' "light2:', ' "light2":')
        dat = dat.replace(' light2":', ' "light2":')
        dat = dat.replace(' "light2"1', ' "light2":1')
        dat = dat.replace(' "light2"2', ' "light2":2')
        dat = dat.replace(' "light2"3', ' "light2":3')
        dat = dat.replace(' "light2"4', ' "light2":4')
        dat = dat.replace(' "light2"5', ' "light2":5')
        dat = dat.replace(' "light2"6', ' "light2":6')
        dat = dat.replace(' "light2"7', ' "light2":7')
        dat = dat.replace(' "light2"8', ' "light2":8')
        dat = dat.replace(' "light2"9', ' "light2":9')
        dat = dat.replace(' "light2"0', ' "light2":0')
        dat = dat.replace('light2', 'ext_pin')
        #10
        dat = dat.replace('"xt_pin"', '"ext_pin":')
        dat = dat.replace('"et_pin"', '"ext_pin":')
        dat = dat.replace('"ex_pin"', '"ext_pin":')
        dat = dat.replace('"extpin"', '"ext_pin":')
        dat = dat.replace('"ext_in"', '"ext_pin":')
        dat = dat.replace('"ext_pn"', '"ext_pin":')
        dat = dat.replace('"ext_pi"', '"ext_pin":')
        #
        dat = dat.replace(' "ext_pin:', ' "ext_pin":')
        dat = dat.replace(' ext_pin":', ' "ext_pin":')
        dat = dat.replace(' "ext_pin"2', ' "ext_pin":2')
        #10.5
        if ( dat.count('"light"') > 1 ):        #light2 losed '2'
                l = dat.split('"light"', 3)
                dat = l[0] + '"light"' + l[1] +'"ext_pin"' + l[2]
        #
        dat = dat.replace('0\n ', '0,\n ')
        dat = dat.replace('1\n ', '1,\n ')
        dat = dat.replace('2\n ', '2,\n ')
        dat = dat.replace('3\n ', '3,\n ')
        dat = dat.replace('4\n ', '4,\n ')
        dat = dat.replace('5\n ', '5,\n ')
        dat = dat.replace('6\n ', '6,\n ')
        dat = dat.replace('7\n ', '7,\n ')
        dat = dat.replace('8\n ', '8,\n ')
        dat = dat.replace('9\n ', '9,\n ')
        #
        dat = dat.replace('\n,\n', '\n},\n')
        dat = dat.replace('"\n "', '",\n "')
        dat = dat.replace('}\n{', '},\n{')
        dat = dat.replace('::', ':')
        #print(': count', dat.count(':') )
        if (dat.count(':') != 12):
                '''for i in range(1000000):
                        s = str(i)
                        for k in range(6 - len(s)):
                                s = '0' +s
                        dat = dat.replace(' '+s[0:2] +':' +s[2:6], ' '+s[0:2] +':' +s[2:4] +':' +s[4:6])
                        dat = dat.replace(' '+s[0:4] +':' +s[4:6], ' '+s[0:2] +':' +s[2:4] +':' +s[4:6])'''
                #
                se = 0
                mi = 0
                ho = 0
                for i in range(86400):
                        ho = math.floor( i/ 3600)
                        mi = math.floor( (i - ho*3600)/ 60)
                        se = (i - ho*3600) % 60
                        if (ho < 10):
                                s = '0' +str(ho)
                        else:
                                s = str(ho)
                        if (mi < 10):
                                s = s +'0' +str(mi)
                        else:
                                s =  s +str(mi)
                        if (se < 10):
                                s =  s +'0' +str(se)
                        else:
                                s =  s +str(se)
                        #print('s=', s, ' '+s[0:2] +':' +s[2:6], ' '+s[0:2] +':' +s[2:4] +':' +s[4:6])
                        dat = dat.replace(' '+s[0:2] +':' +s[2:6], ' '+s[0:2] +':' +s[2:4] +':' +s[4:6])
                        dat = dat.replace(' '+s[0:4] +':' +s[4:6], ' '+s[0:2] +':' +s[2:4] +':' +s[4:6])
        dat = dat.replace('', '')
        return dat



'''
        0       "id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE ,\
        1       "dirt" INTEGER , \
        2       "timing" INTEGER , \
        3       "datetime" DATETIME NOT NULL  DEFAULT (DATETIME()) , \
        4       "pressure" FLOAT, \
        5       "temp1" FLOAT, \
        6       "temp2" FLOAT , \
        7       "dew_point" FLOAT, \
        8       "huminity" FLOAT , \
        9       "light" FLOAT , \
        10      "ext_pin" FLOAT );' )
'''

def saveDataList(dataList):
        for dataBlob in dataList:
                if (dataBlob.get("cmd", None) != None):
                        continue        #skip commands
                if (dataBlob.get("temp1") == None):
                         dataBlob["temp1"] = None
                if ( len(dataBlob.get("datetime", '')) < 19 ):
                        print( "broken date skipping!", dataBlob.get("datetime", '') )
                        continue
                print(dataBlob)
                #make a copy
                n_dataBlob = {}
                for a in ("dirt", "timing", "datetime", "pressure", "temp1", "temp2", "dew_point","huminity", "light", "ext_pin" ):
                        n_dataBlob[a] = dataBlob.get(a)
                        if (n_dataBlob[a] == None):
                                print("found Null:", a)
                dataBlob = n_dataBlob
                cur.execute('INSERT INTO "Mansikka" (id, dirt, timing, datetime, pressure, temp1, temp2, dew_point, huminity, light, ext_pin )\
                VALUES (\
                NULL, \
                :dirt, \
                :timing, \
                :datetime, \
                :pressure, \
                :temp1, \
                :temp2, \
                :dew_point, \
                :huminity, \
                :light, \
                :ext_pin )', dataBlob )
                db.commit()
                saveToCSV(dataBlob)

def saveToCSV(dataObj):
        f = open('iot.csv', 'a+')
        #
        for a in ("dirt", "timing", "datetime", "pressure", "temp1", "temp2", "dew_point","huminity", "light", "ext_pin" ):
                val = dataObj.get(a, '')
                if (val == None):
                        val = ''
                f.write( str(val) )
                f.write(',')
        f.write('\n')
        f.close()



def checkMissingFields(blob):
        for dataObj in blob:
                if (dataObj.get("cmd", None) != None):
                        continue        #skip commands
                for a in ("dirt", "timing", "datetime", "pressure", "temp1", "temp2", "dew_point","huminity", "light", "ext_pin" ):
                        try:
                                del(dataObj[a])
                        except:
                                print("Fix:", a)
                                continue
                if (len(dataObj) > 0):
                        print( "Broken:", dataObj)


def checkDate(blob):
        count = 0
        for dataObj in blob:
                dt = dataObj.get("datetime", "")
                if ( len(dt) < 19):
                        print( "broken date!", dt )
                        count = count +1
        print("found", count, "broken date!")








def writeSerial(text):
    global ser
    ser.write(str.encode(text))


def sendCommands():
    '''sendCommands

    read file listing commands and send them to client
    '''
    try:
        f = open('commandsToSend.txt', 'r+')
    except:
        return
    k = f.readline()
    while(len(k) != 0):
        print( "sending cmd: {0}".format(k) )
        writeSerial("{0}\n".format(k))
        k = f.readline()
    f.close()
    f = open('commandsToSend.txt', 'w')
    f.close()





''' Main loop
'''
full_list = []
while(1):
        # send commands to client
        sendCommands()
        # find start char '{'
        read_str = readSerial()
        if (read_str == ""):
                continue
        read_str = old_str + read_str
        old_str = read_str
        #
        start_index = read_str.find("{")
        if (start_index == -1):
                continue
        # find end chars "},"
        
        end_index = read_str.find("},")
        if (end_index == -1):
                continue
        elif (end_index < start_index):
                # we have ending of data but no head, trim it out
                old_str = read_str[start_index:-1]
                continue
        end_index = end_index+2         # inglude '},'
        old_str = read_str[end_index:-1]        # save end_bytes
        data_chunk = read_str[start_index:end_index-1]  # read string as json
        # fix old name
        data_chunk = data_chunk.replace(' "light2":', ' "ext_pin":')
        saveList = []
        data_chunk = tryFixingData( data_chunk.replace('\r','') )
        data_chunk = data_chunk.replace('\n','').replace('\r','').replace('\\n','').replace('\\r','')
        try: 
            obj = json.loads( '[' +data_chunk +']' )
            saveList = obj
            if ( obj[0].get('cmd') ):
                print( data_chunk )
        except:
                print( data_chunk )
                print("[warning] impossible!")
        #checkMissingFields( saveList )
        saveDataList( saveList )
        full_list.extend(saveList)

checkDate(full_list)


